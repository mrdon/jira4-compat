This library helps a JIRA 5 targeted plugin run in JIRA 4.2 or later.

To use:

1. Add this jar to your plugin in its expanded form.  For a plugin using the Atlassian SDK, this means adding
the library to your pom.xml:

  <dependency>
    <groupId>com.atlassian.labs</groupId>
    <artifactId>jira4-compat</artifactId>
    <version>LATEST_VERSION</version>
  </dependency>

2. Ensure the library will be expanded into your project by configuring the maven-jira-plugin (or maven-amps-plugin):

  <configuration>
  ...
    <extractDependencies>true</extractDependencies>
  </configuration>

This is necessary as the library contains a Spring XML file that needs to get loaded for your plugin.

3. Expose the module types that your plugin needs with a unique name.  For example, here we expose the 
CompatViewProfilePanelModuleDescriptor in our atlassian-plugin.xml then use it later:

  <module-type key="myplugin-view-profile-panel" class="com.atlassian.labs.jira4compat.CompatViewProfilePanelModuleDescriptor"/>
  ...
  <myplugin-view-profile-panel key="my-panel" name="My Panel" class="com.example.MyCompatViewProfilePanel">
    <order>30</order>
  </myplugin-view-profile-panel>

It is important to choose a unique name as module types are shared among all plugins.  This means if you define the type in one 
of your plugins, you could re-use it in another.
