package com.atlassian.labs.jira4compat.spi;

import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.labs.jira4compat.api.CompatViewProfilePanel;
import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.module.ModuleFactory;

/**
 *
 */
public interface CompatViewProfilePanelFactory
{
    Object convert(CompatViewProfilePanel panel);

    ModuleDescriptor createViewProfilePanelModuleDescriptor(JiraAuthenticationContext context, ModuleFactory moduleFactory);
}
