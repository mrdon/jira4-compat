package com.atlassian.labs.jira4compat.spi;

import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.labs.jira4compat.api.CompatIssueTabPanel;
import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.module.ModuleFactory;

/**
 *
 */
public interface CompatIssueTabPanelFactory
{
    Object convert(CompatIssueTabPanel panel);

    ModuleDescriptor createIssueTabPanelModuleDescriptor(JiraAuthenticationContext context, ModuleFactory moduleFactory);
}
