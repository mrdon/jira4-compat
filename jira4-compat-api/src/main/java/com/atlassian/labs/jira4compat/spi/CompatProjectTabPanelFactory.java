package com.atlassian.labs.jira4compat.spi;

import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.labs.jira4compat.api.CompatProjectTabPanel;
import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.module.ModuleFactory;

/**
 *
 */
public interface CompatProjectTabPanelFactory
{
    Object convert(CompatProjectTabPanel panel);

    ModuleDescriptor createProjectTabPanelModuleDescriptor(JiraAuthenticationContext context, ModuleFactory moduleFactory);
}
