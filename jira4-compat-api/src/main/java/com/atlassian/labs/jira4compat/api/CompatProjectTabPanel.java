package com.atlassian.labs.jira4compat.api;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.project.Project;
import com.atlassian.plugin.ModuleDescriptor;

/**
 *
 */
public interface CompatProjectTabPanel
{
    /**
     * Initialize the tab panel panel with the plugins ProjectTabPanelModuleDescriptor.  This is usually used for
     * rendering velocity views.
     *
     * @param descriptor the descriptor for this module as defined in the plugin xml descriptor.
     */
    void init(ModuleDescriptor descriptor);

    /**
     * Used to render the tab.
     * Note that this pulls apart the normal BrowseContext and only adds in the parameters that Bonfire requires.
     *
     * @return Escaped string with the required html.
     */
    String getHtml(User user, Project project);

    /**
     * Determine whether or not to show this.
     * Note, this doesn't pass additional information as Bonfire doesn't require it.
     *
     * @return True if the conditions are right to display tab, otherwise false.
     */
    boolean showPanel();
}
